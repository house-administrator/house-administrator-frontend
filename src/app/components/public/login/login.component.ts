import { Component, OnInit } from '@angular/core';
import { AuthUserService } from 'src/app/services/auth-user.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userFormGroup: FormGroup;
  submitted: any = false;

  constructor(
    private authUserService: AuthUserService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    
  }

  onSubmit() {
    this.submitted = true;
    if (this.userFormGroup.invalid) {
      return;
    }
  }

  formValue() {
    return this.userFormGroup.value;
  }

  formControls() {
    return this.userFormGroup.controls;
  }

}
