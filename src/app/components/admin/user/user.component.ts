import { Component, OnInit } from '@angular/core';
import { WebService } from 'src/app/services/web.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(
    private webService: WebService,
    private userService: UserService

  ) { }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.userService.getAll().subscribe(
      (res) => {
        console.log('Este es el resultado del backend --> ', res);
      }
    )
  }

}
