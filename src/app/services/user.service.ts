import { Injectable } from '@angular/core';
import { SERVER } from '../config/server.config';
import { WebService } from './web.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private webService: WebService
  ) { }

  getAll() {
    const URL = SERVER.URL_BASE + '/users'
    return this.webService.get(URL);
  }
  
}
