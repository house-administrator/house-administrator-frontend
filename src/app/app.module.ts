import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/public/login/login.component';
import { SignupComponent } from './components/public/signup/signup.component';
import { DashboradComponent } from './components/public/dashborad/dashborad.component';
import { AboutUsComponent } from './components/public/about-us/about-us.component';
import { CompanyComponent } from './components/admin/company/company.component';
import { UserComponent } from './components/admin/user/user.component';
import { ProductComponent } from './components/admin/product/product.component';
import { PlateComponent } from './components/admin/plate/plate.component';
import { MealComponent } from './components/admin/meal/meal.component';
import { SidebarComponent } from './components/admin/sidebar/sidebar.component';
import { NavbarComponent } from './components/public/navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    DashboradComponent,
    AboutUsComponent,
    CompanyComponent,
    UserComponent,
    ProductComponent,
    PlateComponent,
    MealComponent,
    SidebarComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
