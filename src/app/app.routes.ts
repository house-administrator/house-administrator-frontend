import { Routes } from '@angular/router';
import { UserComponent } from './components/admin/user/user.component';
import { DashboradComponent } from './components/public/dashborad/dashborad.component';
import { AboutUsComponent } from './components/public/about-us/about-us.component';
import { LoginComponent } from './components/public/login/login.component';
import { SignupComponent } from './components/public/signup/signup.component';
import { NavbarComponent } from './components/public/navbar/navbar.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashborad',
        pathMatch: 'full'   
    },
    {
        path: '',
        component: NavbarComponent,
        outlet: 'navbar'
    },
    {
        path: 'dashborad',
        component: DashboradComponent,
    },
    {
        path: 'about-us',
        component: AboutUsComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'signup',
        component: SignupComponent
    },
    {
        path: 'users',
        component: UserComponent
    }
];